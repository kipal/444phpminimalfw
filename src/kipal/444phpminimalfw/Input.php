<?

namespace kipal\fw;

interface Input
{
	static function createFromInput(InputHandler $ish);

	function getParams($key, $default = null);

	function getController() : string;

	function getAction() : string;
}
