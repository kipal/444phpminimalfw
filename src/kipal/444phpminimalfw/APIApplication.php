<?

namespace kipal\fw;

class APIApplication extends Application
{
	public static function errorAPIResponse(\Exception $ex)
	{
		$r = new APIResponse();
		$r->set(
			"error",
			[
				"exception" => get_class($ex),
				"message" => $ex->getMessage()
			]
		);

		echo $r->render();
	}
}
