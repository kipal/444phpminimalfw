<?

namespace kipal\fw;

class APIRequest implements Input
{
	protected $controller = "";

	protected $action = "";

	protected $params = [];

	public function __construct(string $controller, string $action, array $params = [])
	{
		$this->controller = $controller;
		$this->action = $action;
		$this->params = $params;
	}

	public static function createFromInput(InputHandler $ish) : APIRequest
	{
		$jsonArray = json_decode($ish->readAll(), true);

		if (!isset($jsonArray["controller"])) {

			throw new MissingParameterException("'controller' key is missing.");
		}

		if (!isset($jsonArray["action"])) {

			throw new MissingParameterException("'action' key is missing.");
		}

		if (!isset($jsonArray["data"])) {

			throw new MissingParameterException("'data' key is missing.");
		}

		$req = new static($jsonArray["controller"], $jsonArray["action"], $jsonArray["data"]);

		return $req;
	}

	public function getParams($key, $default = null)
	{

		if (!isset($this->params[$key])) {

			throw new MissingParameterException("'$key' key is missing.");
		}

		return $this->params[$key];
	}

	public function getController(): string
	{

		return $this->controller;
	}

	public function getAction(): string
	{

		return $this->action;
	}

}
