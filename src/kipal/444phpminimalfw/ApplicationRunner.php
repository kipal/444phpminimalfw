<?

namespace kipal\fw;

use kipal\fw\Input;

class ApplicationRunner
{
	protected $errorCallback = null;

	public function setErrorHandler($callback)
	{
		$this->errorCallback = $callback;

		return $this;
	}

	public function execute(array $config, string $applicationClass, Input $input)
	{
		try{
			$app = new \ReflectionClass($applicationClass);
			$app->getMethod("setConfig")->invoke(null, $config);
			$instance = $app->getMethod("getInstance")->invoke(null);

			if (!($instance instanceof \kipal\fw\Application)) {

				throw new \Exception("'$applicationClass' is not an instance of kipal\\fw\\Application.");
			}

			$app->getMethod("run")->invoke($instance, $input);

		} catch (\Exception $ex) {
			call_user_func($this->errorCallback, $ex);
		}
	}
}
