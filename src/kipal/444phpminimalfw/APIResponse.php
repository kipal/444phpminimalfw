<?

namespace kipal\fw;

class APIResponse implements View
{
	private $jsonArray = [];

	public function set(string $key, $value)
	{
		$this->jsonArray[$key] = $value;
	}

	public function get(string $key)
	{

		return $this->jsonArray[$key];
	}

	public function render() : string
	{

		return json_encode($this->jsonArray);
	}
}
