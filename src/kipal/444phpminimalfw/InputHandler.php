<?

namespace kipal\fw;

interface InputHandler
{
	function write(string $content);

	function readAll() : string;
}
