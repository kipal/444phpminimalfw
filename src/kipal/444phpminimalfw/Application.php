<?

namespace kipal\fw;

abstract class Application
{
	// In this way we still can create other kind of applications
	// during runtime
	protected static $instance = null;

	protected static $c = [];

	protected $components = [];

	protected function lookForControllerInClassMap(string $key) : string
	{
		if (isset(static::$c["controllerMap"][$key])) {

			return static::$c["controllerMap"][$key];
		}

		return "";
	}

	public function getConfig(string $key)
	{
		if (!isset(static::$c[$key])) {

			throw new MissingParameterException("'$key' is missing from config.");
		}

		return static::$c[$key];
	}

	public function run(Input $r)
	{
		$controllerName = $r->getController();
		$controllerNameFromMap = $this->lookForControllerInClassMap($controllerName);
		if ("" === $controllerNameFromMap) {
			$controllerName = ucfirst($controllerName) . "Controller";
		} else {
			$controllerName = $controllerNameFromMap;
		}

		if (!class_exists($controllerName)) {

			throw new NoClassFoundException("There's no '$controllerName' class.");
		}

		$controller = new $controllerName();

		$actionName = $r->getAction();
		$actionName = "action" . ucfirst($actionName);

		if (!method_exists($controller, $actionName)) {

			throw new NoMethodFoundException("There's no '$actionName' action in '$controllerName' controller.");
		}

		$view = $controller->$actionName($r);

		if (!($view instanceof View)) {

			throw new WrongResponseException();
		}

		echo $view->render();
	}

	public static function setConfig(array $c)
	{
		if (!isset($c["env"])){

			throw new MissingParameterException("'env' is missing from config.");
		}

		static::$c = $c;
	}

	protected static function init()
	{
		// TODO test: if there's any component
		if (isset(static::$c["components"])) {
			static::initComponents();
		}
	}

	private static function initComponents()
	{
		foreach (static::$c["components"] as $name => $component) {
			// TODO test
			if (!isset($component["class"])) {

			}

			static::$instance->components[$name] = new $component["class"];

			unset($component["class"]);

			static::$instance->components[$name]->setMembers($component);
			static::$instance->components[$name]->init();

		}
	}

	public static function getInstance()
	{
		if (empty(static::$instance)) {

			if (empty(static::$c)) {

				throw new NoConfigException();
			}

			static::$instance = new static();
		}

		static::init();

		return static::$instance;
	}

	public static function removeInstance()
	{
		static::$instance = null;
	}

	public function getComponent(string $c) : Component
	{
		//TODO test for undefined index
		if (!isset(static::$instance->components[$c])) {

		}

		return static::$instance->components[$c];
	}
}
