<?

namespace kipal\fw;

interface View
{
	function render() : string;
}
