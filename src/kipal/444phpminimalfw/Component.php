<?

namespace kipal\fw;

abstract class Component
{
	private $members = [];

	public function __call($key, $arguments)
	{
		$key = lcfirst(preg_replace("/get/", "", $key));
		// TODO test
		if (!isset($this->members[$key])) {
		}

		return $this->members[$key];
	}

	public function setMembers(array $members)
	{
		$this->members = $members;
	}

	public function init()
	{
	}
}
