<?

namespace kipal\fw;

class CLIParams implements Input
{
	private $controller = "";

	private $action = "";

	public function __construct(string $controller, string $action)
	{
		$this->controller = $controller;
		$this->action = $action;
	}

	public static function createFromInput(InputHandler $ish) : APIRequest
	{
		$req = new static("", "");

		return $req;
	}

	public function getParams($key, $default = null)
	{
	}

	public function getController(): string
	{

		return $this->controller;
	}

	public function getAction(): string
	{

		return $this->action;
	}
}
