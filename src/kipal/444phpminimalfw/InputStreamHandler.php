<?

namespace kipal\fw;

class InputStreamHandler implements InputHandler
{
	protected $address = "";

	public function __construct(string $address)
	{
		$this->address = $address;
	}

	public function write(string $content)
	{
		file_put_contents($this->address, $content);
	}

	public function readAll() : string
	{

		return file_get_contents($this->address);
	}
}
