<?

namespace kipal\fw;

class CLIResponse implements View
{
	private $message = "";

	public function setMessage(string $message)
	{
		$this->message = $message;

		return $this;
	}

	public function render() : string
	{

		return $this->message;
	}
}
