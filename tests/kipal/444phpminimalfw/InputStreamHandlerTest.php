<?

use PHPUnit\Framework\TestCase;
use kipal\fw\InputStreamHandler;

class InputStreamHandlerTest extends TestCase
{
	public function testFirst()
	{
		$ish = new InputStreamHandler("test");

		$ish->write("something");

		$this->assertEquals("something", $ish->readAll());
	}
}
