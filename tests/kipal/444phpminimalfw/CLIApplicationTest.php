<?

use PHPUnit\Framework\TestCase;
use kipal\fw\CLIApplication;
use kipal\fw\CLIParams;
use kipal\fw\CLIResponse;

class TestCLIController
{
	public function actionTest() : CLIResponse
	{

		$cr = new CLIResponse();
		$cr->setMessage("OK");

		return $cr;
	}
}


class CLIApplicationTest extends TestCase
{
	public function testRun()
	{
		CLIApplication::setConfig(["env" => "dev"]);
		//CLIApplication::getInstance()->run(CLIParams::createFromInputStream(new CLIParamsHandler()));

		ob_start();
		CLIApplication::getInstance()->run(new CLIParams("testCLI", "test"));

		$this->assertEquals("OK", ob_get_clean());
	}
}
