<?

use PHPUnit\Framework\TestCase;
use kipal\fw\CLIResponse;

class CLIResponseTest extends TestCase
{
	public function testRender()
	{
		$r = new CLIResponse();

		$r->setMessage("testMessage");

		$this->assertEquals(
			"testMessage",
			$r->render()
		);
	}
}
