<?

use PHPUnit\Framework\TestCase;
use kipal\fw\APIResponse;

class APIResponseTest extends TestCase
{
	public function testSetterGetter()
	{
		$r = new APIResponse();

		$r->set("testKey", "testValue");

		$this->assertEquals("testValue", $r->get("testKey"));
	}

	public function testRender()
	{
		$r = new APIResponse();

		$r->set("testKey1", "testValue1");
		$r->set("testKey2", "testValue2");

		$this->assertEquals(
			"{\"testKey1\":\"testValue1\",\"testKey2\":\"testValue2\"}",
			$r->render()
		);
	}
}
