<?

use PHPUnit\Framework\TestCase;
use kipal\fw\APIApplication;
use kipal\fw\APIRequest;
use kipal\fw\APIResponse;
use kipal\fw\InputStreamHandler;
use kipal\fw\Component;

class TestController
{
	protected static $numberOfInstances = 0;

	public function __construct()
	{
		static::$numberOfInstances++;
	}

	public static function getNumberOfInstances() : int
	{

		return static::$numberOfInstances;
	}

	public function actionTest(APIRequest $r) : APIResponse
	{

		$r = new APIResponse();
		$r->set("test", "test");

		return $r;
	}

	public function actionTestWrong(APIRequest $r) : int
	{

		return 0;
	}
}

class NotTestController extends TestController
{
	protected static $numberOfInstances = 0;
}

class APIApplicationTest extends TestCase
{
	protected function tearDown()
	{
		APIApplication::removeInstance();
	}

	public static function tearDownAfterClass()
	{
		$file = "test";
		if (file_exists($file)) {
			unlink($file);
		}
	}

	private function checkOutputBuffer(array $expected)
	{
		$this->assertEquals(json_encode(["error" => $expected]), ob_get_clean());
	}

	public function testErrorAPIResponse()
	{
		ob_start();
		APIApplication::errorAPIResponse(new \Exception("test"));
		$this->checkOutputBuffer([
			"exception" => "Exception",
			"message" => "test"
		]);
	}

	/**
	 *@expectedException kipal\fw\NoConfigException
	 */
	public function testConfigEmpty()
	{
		APIApplication::getInstance();
	}

	/**
	 *@expectedException kipal\fw\MissingParameterException
	 */
	public function testConfigNoEnv()
	{
		APIApplication::setConfig([]);
	}

	public function testRun()
	{
		ob_start();
		APIApplication::setConfig(["env" => "dev"]);

		$this->assertEquals("dev", APIApplication::getInstance()->getConfig("env"));

		APIApplication::getInstance()->run(new APIRequest("test", "test"));

		$this->assertEquals(1, TestController::getNumberOfInstances());
		$this->assertEquals(
			"{\"test\":\"test\"}",
			ob_get_clean()
		);
	}

	/**
	 *@expectedException kipal\fw\NoClassFoundException
	 */
	public function testRunNoClassFound()
	{
		APIApplication::setConfig(["env" => "dev"]);
		APIApplication::getInstance()->run(new APIRequest("testAnything", ""));
	}

	/**
	 *@expectedException kipal\fw\NoMethodFoundException
	 */
	public function testRunNoMethodFound()
	{
		APIApplication::setConfig(["env" => "dev"]);
		APIApplication::getInstance()->run(new APIRequest("test", "testAnything"));
	}

	/**
	 *@expectedException kipal\fw\WrongResponseException
	 */
	public function testRunWrongResponse()
	{
		APIApplication::setConfig(["env" => "dev"]);
		APIApplication::getInstance()->run(new APIRequest("test", "testWrong"));
	}

	public function testComponentsFromConfig()
	{
		APIApplication::setConfig(
			[
				"env" => "dev",
				"components" => [
					"test" => [
						"class" => "TestComponent",
						"attr1" => "value1",
						"attr2" => "value2"
					]
				]
			]
		);

		$tc = APIApplication::getInstance()->getComponent("test");
		$this->assertInstanceOf("kipal\\fw\\Component", $tc);
		$this->assertEquals("value1", $tc->getAttr1());
		$this->assertEquals("value2", $tc->getAttr2());
		$this->assertEquals(1, $tc->initHasCalled);
	}

	public function testRouteToController()
	{
		ob_start();
		APIApplication::setConfig(
			[
				"env" => "dev",
				"controllerMap" => [
					"test" => "NotTestController"
				]
			]
		);
		APIApplication::getInstance()->run(new APIRequest("test", "test"));
		$this->assertEquals(
			"{\"test\":\"test\"}",
			ob_get_clean()
		);

		$this->assertEquals(1, NotTestController::getNumberOfInstances());
	}
}
