<?

use PHPUnit\Framework\TestCase;
use kipal\fw\ApplicationRunner;
use kipal\fw\Application;
use kipal\fw\Input;
use kipal\fw\APIRequest;

class TestApplication extends Application
{
	private static $throwException = false;

	public static $numberOfThrownExceptions = 0;

	public function run(Input $r)
	{
		if (static::$throwException) {

			throw new \Exception("test");
		}

		parent::run($r);
	}

	public static function errorHandling(\Exception $ex)
	{
		static::$numberOfThrownExceptions++;
	}

	public static function setThrowException(bool $v)
	{
		static::$throwException = $v;
	}
}

class ApplicationRunnerTest extends TestCase
{
	public function tearDown()
	{
		TestApplication::removeInstance();
	}

	public function testRun()
	{
		$ar = new ApplicationRunner();
		ob_start();
		$ar->setErrorHandler("TestApplication::errorHandling")
		->execute(
			["env" => "dev"],
			"TestApplication",
			new APIRequest("test", "test")
		);

		$this->assertEquals("{\"test\":\"test\"}", ob_get_clean());
		$this->assertEquals(0, TestApplication::$numberOfThrownExceptions);

		TestApplication::setThrowException(true);
		$ar->execute(
			["env" => "dev"],
			"TestApplication",
			new APIRequest("test", "test")
		);

		$this->assertEquals(1, TestApplication::$numberOfThrownExceptions);

	}
}
