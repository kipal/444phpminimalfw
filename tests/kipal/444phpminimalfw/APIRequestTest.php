<?

use PHPUnit\Framework\TestCase;
use kipal\fw\APIRequest;
use kipal\fw\InputStreamHandler;

class APIRequestTest extends TestCase
{
	public static function tearDownAfterClass()
	{
		$file = "test";
		if (file_exists($file)) {
			unlink($file);
		}
	}

	public function testJSONParams()
	{
		$ish = new InputStreamHandler("test");
		$ish->write("{\"controller\":\"testController\",\"action\":\"testAction\",\"data\":{\"test\":\"apple\"}}");

		$r = APIRequest::createFromInput($ish);

		$this->assertEquals("apple", $r->getParams("test"));
	}

	/**
	 * @expectedException \kipal\fw\MissingParameterException
	 */
	public function testControllerName()
	{
		$ish = new InputStreamHandler("test");
		$ish->write("{\"controller\":\"testController\",\"data\":{}}");

		$r = APIRequest::createFromInput($ish);

		$this->assertEquals("testController", $r->getController());

		$ish->write("{}");

		$r = APIRequest::createFromInput($ish);
		$r->getController();
	}

	/**
	 * @expectedException \kipal\fw\MissingParameterException
	 */
	public function testActionName()
	{
		$ish = new InputStreamHandler("test");
		$ish->write("{\"action\":\"testAction\",\"data\":{}}");

		$r = APIRequest::createFromInput($ish);

		$this->assertEquals("testAction", $r->getAction());

		$ish->write("{}");

		$r = APIRequest::createFromInput($ish);
		$r->getAction();
	}

	/**
	 *@expectedException kipal\fw\MissingParameterException
	 */
	public function testMissingDataKey()
	{
		$ish = new InputStreamHandler("test");
		$r = APIRequest::createFromInput($ish);

		$r->getParams("anything");
	}

	/**
	 *@expectedException kipal\fw\MissingParameterException
	 */
	public function testMissingKeyInDataArray()
	{
		$ish = new InputStreamHandler("test");
		$ish->write("{\"data\":{\"test\":\"apple\"}}");

		$r = APIRequest::createFromInput($ish);

		$r->getParams("test2");
	}
}
