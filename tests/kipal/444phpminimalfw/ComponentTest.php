<?

use PHPUnit\Framework\TestCase;
use kipal\fw\Component;

class TestComponent extends Component
{
	public $initHasCalled = 0;

	public function init()
	{
		$this->initHasCalled++;
	}
}

class ComponentTest extends TestCase
{
	public function testGetAttrFromArrayByMagicMethod()
	{
		$c = new TestComponent();

		$c->setMembers(["test" => "test"]);

		$c->init();

		$this->assertEquals("test", $c->getTest());
	}
}
